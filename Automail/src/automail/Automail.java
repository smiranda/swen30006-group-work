package automail;

import simulation.IMailDelivery;

public class Automail {
	      
    public Robot[] robots;
    public MailPool mailPool;
    
    public Automail(MailPool mailPool, IMailDelivery delivery, int numRobots, StatisticsLog statisticsLog, ChargeCalculator chargeCalculator ) {

        /** Initialize the MailPool */
    	this.mailPool = mailPool;
    	
    	/** Initialize robots */
    	robots = new Robot[numRobots];
    	for (int i = 0; i < numRobots; i++){
            robots[i] = new Robot(delivery, mailPool, i);

            // Each robots gets personal device to access statistics and charge calculator
            RobotDevice newDevice = new RobotDevice(statisticsLog, chargeCalculator);
            robots[i].device = newDevice;

        }
    }

}
