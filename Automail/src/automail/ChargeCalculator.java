package automail;

import com.unimelb.swen30006.wifimodem.WifiModem;
import simulation.Building;
import simulation.Simulation;

import java.util.HashMap;

public class ChargeCalculator {

    public final WifiModem wifiModem;
    private final double activityUnitPrice;
    private final double markupPercentage;

    private final HashMap<Integer, Double> floorServiceFeeRecord;

    public ChargeCalculator(WifiModem wifiModem, double activityUnitPrice, double markupPercentage) {
        this.wifiModem = wifiModem;
        this.activityUnitPrice = activityUnitPrice;
        this.markupPercentage = markupPercentage;
        this.floorServiceFeeRecord = new HashMap<>();
    }

    public double calculateCharge(MailItem item) {
        int destination = item.destination_floor;
        double activityUnits = ((2*(destination - Building.MAILROOM_LOCATION)) * Simulation.MOVEMENT_COST) + Simulation.LOOKUP_COST;
        item.setActivityUnitsUtilized(activityUnits);
        double activityCost = activityUnits * activityUnitPrice;
        item.setActivityCost(activityCost);

        double serviceFee = 0.00;
        double latestServiceFee = wifiModem.forwardCallToAPI_LookupPrice(destination);
        if (latestServiceFee >= 0) {
            item.setSuccessfulLookupCount(item.getSuccessfulLookupCount()+1);
            item.setServiceFee(latestServiceFee);
            floorServiceFeeRecord.put(destination,latestServiceFee);
            serviceFee = latestServiceFee;
        } else if (floorServiceFeeRecord.containsKey(destination)) {
            serviceFee = floorServiceFeeRecord.get(destination);
            item.setServiceFee(serviceFee);
        }
        double cost = (activityCost + serviceFee);
        double charge = cost * (1 + markupPercentage);
        item.setCharge(charge);
        return charge;
    }

    public double getActivityUnitPrice() {
        return activityUnitPrice;
    }
}
