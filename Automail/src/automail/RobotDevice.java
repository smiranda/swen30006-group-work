package automail;

import simulation.Simulation;

public class RobotDevice {

    public final StatisticsLog statisticsLog;
    public final ChargeCalculator chargeCalculator;

    public RobotDevice(StatisticsLog statisticsLog, ChargeCalculator chargeCalculator) {
        this.statisticsLog = statisticsLog;
        this.chargeCalculator = chargeCalculator;
    }

    public double calculateFinalCharge (MailItem item) {
        double final_charge = chargeCalculator.calculateCharge(item) - Simulation.LOOKUP_COST;
        return final_charge;
    }

    public void recordDeliverySuccess(MailItem item) {
        double activity_unit_price = chargeCalculator.getActivityUnitPrice();
        statisticsLog.recordDeliverySuccess(item, activity_unit_price);
    }

}
