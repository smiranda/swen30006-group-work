package automail;

import simulation.Building;
import simulation.Simulation;

public class StatisticsLog {
    private int totalItemsDelivered;
    private double totalBillableActivity;
    private double totalActivityCost;
    private double totalServiceCost;
    private int numSuccessfulLookup;
    private int numFailedLookup;
    private int totalLookup;

    private final int ONE_LOOKUP = 1;


    public void recordDeliverySuccess(MailItem deliveryItem, double activityUnitPrice) {
        totalItemsDelivered++;
        totalBillableActivity = totalBillableActivity + 2*(deliveryItem.getDestFloor() - Building.MAILROOM_LOCATION) + ONE_LOOKUP;
        totalActivityCost = totalBillableActivity * activityUnitPrice;
        totalServiceCost = totalServiceCost + deliveryItem.getServiceFee();
        numSuccessfulLookup = numSuccessfulLookup + deliveryItem.getSuccessfulLookupCount();
        numFailedLookup = numFailedLookup + (2 - deliveryItem.getSuccessfulLookupCount());
        totalLookup = numSuccessfulLookup+numFailedLookup;
    }

    @Override
    public String toString() {
        return String.format("Total items delivered: %d \n" +
                "Total billable activity: %.2f \n" +
                "Total activity cost: %.2f \n" +
                "Total service cost: %.2f \n" +
                "Total failed lookups: %d \n" +
                "Total successful lookups: %d \n" +
                "Total lookups: %d",totalItemsDelivered, totalBillableActivity, totalActivityCost, totalServiceCost,
                numFailedLookup, numSuccessfulLookup, totalLookup );
    }
}
