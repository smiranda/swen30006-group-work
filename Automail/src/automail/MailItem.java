package automail;

import java.util.Map;
import java.util.TreeMap;

// import java.util.UUID;

/**
 * Represents a mail item
 */
public class MailItem {
	
    /** Represents the destination floor to which the mail is intended to go */
    protected final int destination_floor;
    /** The mail identifier */
    protected final String id;
    /** The time the mail item arrived */
    protected final int arrival_time;
    /** The weight in grams of the mail item */
    protected final int weight;

    /* Holds the financial information associated with item's delivery */
    private double serviceFee;
    private int successfulLookupCount;
    private double activityUnitsUtilized;
    private double charge;
    private double cost;
    private double activityCost;

    /**
     * Constructor for a MailItem
     * @param dest_floor the destination floor intended for this mail item
     * @param arrival_time the time that the mail arrived
     * @param weight the weight of this mail item
     */
    public MailItem(int dest_floor, int arrival_time, int weight){
        this.destination_floor = dest_floor;
        this.id = String.valueOf(hashCode());
        this.arrival_time = arrival_time;
        this.weight = weight;
    }

    public void setServiceFee(double serviceFee){
        this.serviceFee = serviceFee;
    }

    @Override
    // The toString for the older version
    public String toString(){
        return String.format("Mail Item:: ID: %6s | Arrival: %4d | Destination: %2d | Weight: %4d", id, arrival_time, destination_floor, weight);
    }

    // The toString for the new version with the new statistics
    public String toStringv2(){
        return String.format("Mail Item:: ID: %6s | Arrival: %4d | Destination: %2d | Weight: %4d | Charge %.2f " +
                "| Cost: %.2f | Fee: %.2f | Activity: %.2f", id, arrival_time, destination_floor, weight, charge,
                activityCost, serviceFee, activityUnitsUtilized);
    }
    /**
     *
     * @return the destination floor of the mail item
     */
    public int getDestFloor() {
        return destination_floor;
    }
    
    /**
     *
     * @return the ID of the mail item
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @return the arrival time of the mail item
     */
    public int getArrivalTime(){
        return arrival_time;
    }

    /**
    *
    * @return the weight of the mail item
    */
   public int getWeight(){
       return weight;
   }

    public double getServiceFee() {
        return serviceFee;
    }
   
	static private int count = 0;
	static private Map<Integer, Integer> hashMap = new TreeMap<Integer, Integer>();

	@Override
	public int hashCode() {
		Integer hash0 = super.hashCode();
		Integer hash = hashMap.get(hash0);
		if (hash == null) { hash = count++; hashMap.put(hash0, hash); }
		return hash;
	}

    public double getActivityUnitsUtilized() {
        return activityUnitsUtilized;
    }

    public void setActivityUnitsUtilized(double activityUnitsUtilized) {
        this.activityUnitsUtilized = activityUnitsUtilized;
    }

    public double getCharge() {
        return charge;
    }

    public void setCharge(double charge) {
        this.charge = charge;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getActivityCost() {
        return activityCost;
    }

    public void setActivityCost(double activityCost) {
        this.activityCost = activityCost;
    }

    public int getSuccessfulLookupCount() {
        return successfulLookupCount;
    }

    public void setSuccessfulLookupCount(int successfulLookupCount) {
        this.successfulLookupCount = successfulLookupCount;
    }
}
